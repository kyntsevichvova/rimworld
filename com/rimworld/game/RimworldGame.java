package com.rimworld.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.global.biomes.Biomes;
import com.rimworld.engine.map.local.terrain.TerrainPatchMakers;
import com.rimworld.engine.map.local.terrain.TerrainThresholds;
import com.rimworld.engine.map.local.terrain.Terrains;
import com.rimworld.engine.map.local.terrain.rocks.Rocks;
import com.rimworld.engine.world.WorldInfo;
import com.rimworld.game.screens.GameScreen;
import com.rimworld.game.screens.MainMenuScreen;
import com.rimworld.game.screens.StagedScreen;
import com.rimworld.game.screens.newgame.ChooseLandingSite;
import com.rimworld.game.screens.newgame.ChooseScenario;
import com.rimworld.game.screens.newgame.ChooseWorldSeed;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RimworldGame extends Game {
    public final static int MAIN_MENU = 0;
    public final static int CHOOSE_SCENARIO = 1;
    public final static int CHOOSE_WORLD_SEED = 2;
    public final static int CHOOSE_LANDING_SITE = 3;
    public final static int CHOOSE_PAWNS = 4;
    public final static int LOAD_GAME_SCREEN = 5;
    public final static int SETTINGS_SCREEN = 6;
    public final static int GAME_SCREEN = 7;

    public SpriteBatch batch;
    public List<StagedScreen> screens;
    public static TextureAtlas atlas;
    public static TextureAtlas terrainAtlas;
    public static I18NBundle bundle;
    public static Locale locale;
    public static BitmapFont standard;

    public static WorldInfo info;
    public GlobalTile chosenTile;

    public TextButton.TextButtonStyle textButtonStyle;
    public Label.LabelStyle labelStyle;
    public TextField.TextFieldStyle textFieldStyle;
    public Slider.SliderStyle sliderStyle;
    public Window.WindowStyle windowStyle;

    @Override
    public void create() {
        locale = new Locale("ru");
        atlas = new TextureAtlas(Gdx.files.internal("textures/world/biomes/biomesPacked.atlas"));
        terrainAtlas = new TextureAtlas(Gdx.files.internal("textures/terrains/terrainsPacked.atlas"));
        standard = new BitmapFont(Gdx.files.internal("fonts/arial_24.fnt"));
        reloadBundle();

        Terrains.init();
        TerrainThresholds.init();
        TerrainPatchMakers.init();
        Biomes.init();
        Rocks.init();

        createStyles();

        batch = new SpriteBatch();

        screens = new ArrayList<>();

        screens.add(new MainMenuScreen(this));
        screens.add(new ChooseScenario(this));
        screens.add(new ChooseWorldSeed(this));
        screens.add(new ChooseLandingSite(this));
        screens.add(new StagedScreen());
        screens.add(new StagedScreen());
        screens.add(new StagedScreen());
        screens.add(new GameScreen(this));

        showScreen(MAIN_MENU);
    }

    private void createStyles() {
        textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = standard;
        textButtonStyle.up = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/buttonbg.png"))));
        textButtonStyle.down = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/buttonbgclick.png"))));
        textButtonStyle.over = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/buttonbgmouseover.png"))));

        labelStyle = new Label.LabelStyle();
        labelStyle.font = standard;

        textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = standard;
        textFieldStyle.fontColor = new Color(1, 1, 1, 1);
        textFieldStyle.background = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/buttonsubtleatlas.png"))
        ));

        sliderStyle = new Slider.SliderStyle();
        sliderStyle.knob = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/slider_knob.png"))
        ));
        sliderStyle.knobDown = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/slider_knob_active.png"))
        ));
        sliderStyle.knobOver = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/slider_knob_hover.png"))
        ));
        sliderStyle.background = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/buttonsubtleatlas.png"))
        ));

        windowStyle = new Window.WindowStyle();
        windowStyle.titleFont = standard;
        windowStyle.titleFontColor = new Color(1, 1, 1, 1);
        windowStyle.background = new TextureRegionDrawable(new TextureRegion(
                new Texture(Gdx.files.internal("textures/ui/widgets/desbutbg.png"))
        ));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        for (StagedScreen screen : screens) {
            screen.dispose();
        }
    }

    public void showScreen(int screenNum) {
        StagedScreen screen = screens.get(screenNum);
        screen.setInput();
        screen.initialize();
        setScreen(screen);
    }

    private static void reloadBundle() {
        bundle = I18NBundle.createBundle(Gdx.files.internal("i18n/bundle"), locale);
    }
}
