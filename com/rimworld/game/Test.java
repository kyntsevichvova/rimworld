package com.rimworld.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.rimworld.engine.map.global.GlobalMap;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.global.biomes.Biomes;
import com.rimworld.engine.world.WorldInfo;

public class Test extends ApplicationAdapter {
    SpriteBatch batch;
    GlobalMap globalMap;
    OrthographicCamera camera;
    public static TextureAtlas atlas;
    GlobalTile[][] tiles;

    public void create() {
        atlas = new TextureAtlas(Gdx.files.internal("textures/world/biomes/biomesPacked.atlas"));
        Biomes.readDefs();
        batch = new SpriteBatch();
        //world = new World("kek", 1, 1);
        globalMap = new GlobalMap(new WorldInfo("mda", 1, 1));
        tiles = globalMap.tiles;
        camera = new OrthographicCamera(800, 480);
        camera.zoom = 1f;
        camera.setToOrtho(false);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();
    }

    public void render() {
        handleInput();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        for (int i = 0; i < GlobalMap.WORLD_HEIGHT; i++) {
            for (int j = 0; j < GlobalMap.WORLD_WIDTH; j++) {
                if (j * 5 + 5 < camera.position.x - camera.viewportWidth / 2 ||
                        i * 5 + 5 < camera.position.y - camera.viewportHeight / 2) {
                    continue;
                }
                if (j * 5 - 5 > camera.position.x + camera.viewportWidth / 2 ||
                        i * 5 - 5 > camera.position.y + camera.viewportHeight / 2) {
                    continue;
                }
                Sprite s = tiles[i][j].getBiome().sprite;
                s.setPosition(j * 5, i * 5);
                s.draw(batch);
                int x = Gdx.input.getX();
                int y = Gdx.input.getY();
                Vector3 v = camera.unproject(new Vector3(x, y, 0));
                if (v.x >= j * 5 && v.x < j * 5 + 5 &&
                    v.y >= i * 5 && v.y < i * 5 + 5) {
                    Texture texture = new Texture(Gdx.files.internal("textures/world/mousetile.png"));
                    batch.draw(texture, j * 5, i * 5, 5, 5);
                }
            }
        }
        batch.end();
    }

    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.zoom += 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom -= 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-3, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(3, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0, -3, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0, 3, 0);
        }
        camera.zoom = MathUtils.clamp(camera.zoom, 0.01f, 2000.0f / camera.viewportWidth);

        float effectiveViewportWidth = camera.viewportWidth * camera.zoom;
        float effectiveViewportHeight = camera.viewportHeight * camera.zoom;

        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth / 2f, 2000 - effectiveViewportWidth / 2f);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight / 2f, 2000 - effectiveViewportHeight / 2f);
        camera.update();
    }

    public void resize(int width, int height) {

    }

    public void dispose() {

    }
}
