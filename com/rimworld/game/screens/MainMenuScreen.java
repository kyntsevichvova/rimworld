package com.rimworld.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.rimworld.game.RimworldGame;

public class MainMenuScreen extends StagedScreen {

    public MainMenuScreen(RimworldGame game) {
        super(game);
        background = new Sprite(new Texture(Gdx.files.internal("textures/ui/art/bgplanet.png")));
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        background.setPosition(0, 0);
        createMenu();
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        act();
    }

    private void createMenu() {
        Table table = new Table();
        //table.debugAll();
        stage.addActor(table);

        table.setFillParent(true);
        table.right();

        TextButton.TextButtonStyle style = game.textButtonStyle;

        TextButton button;
        button = new TextButton(RimworldGame.bundle.get("newGame"), style);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireNewGame();
            }
        });
        table.add(button).width(180).padRight(80).align(Align.center);

        table.row();
        button = new TextButton(RimworldGame.bundle.get("loadGame"), style);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireLoadGame();
            }
        });
        table.add(button).width(180).padTop(30).padRight(80).align(Align.center);

        table.row();
        button = new TextButton(RimworldGame.bundle.get("settings"), style);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireOpenSettings();
            }
        });
        table.add(button).width(180).padTop(30).padRight(80).align(Align.center);

        table.row();
        button = new TextButton(RimworldGame.bundle.get("exit"), style);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireExitGame();
            }
        });
        table.add(button).width(180).padTop(30).padRight(80).align(Align.center);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        camera.update();
    }

    public void fireNewGame() {
        game.showScreen(RimworldGame.CHOOSE_WORLD_SEED);
    }

    public void fireLoadGame() {
        game.showScreen(RimworldGame.LOAD_GAME_SCREEN);
    }

    public void fireOpenSettings() {
        game.showScreen(RimworldGame.SETTINGS_SCREEN);
    }

    public void fireExitGame() {
        Gdx.app.exit();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
