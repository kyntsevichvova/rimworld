package com.rimworld.game.screens.newgame;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.rimworld.engine.world.WorldInfo;
import com.rimworld.game.RimworldGame;
import com.rimworld.game.screens.StagedScreen;

public class ChooseWorldSeed extends StagedScreen {
    private final static int PREVIOUS_SCREEN = RimworldGame.MAIN_MENU;
    private final static int NEXT_SCREEN = RimworldGame.CHOOSE_LANDING_SITE;

    private TextField seedField;
    private Slider temperatureSlider;
    private Slider humiditySlider;
    private boolean initialized;

    public WorldInfo info;

    public ChooseWorldSeed(RimworldGame game) {
        super(game);
        initialized = false;
        info = new WorldInfo();

        Adapter adapter = new Adapter();
        multiplexer.addProcessor(adapter);
    }

    @Override
    public void initialize() {
        super.initialize();
        if (initialized) {
            return;
        }
        stage.clear();
        createTable();
        initialized = true;
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        act();
    }

    private void createTable() {
        Table table = new Table();
        //table.debugAll();

        Label label = new Label(RimworldGame.bundle.get("worldSeed"), game.labelStyle);
        table.add(label).minWidth(120).height(50);
        TextField textField = new TextField("", game.textFieldStyle);
        table.add(textField).minWidth(120).height(50).padLeft(20);
        table.row();
        seedField = textField;

        label = new Label(RimworldGame.bundle.get("temperature"), game.labelStyle);
        table.add(label).minWidth(120).height(50).padTop(30);
        Slider slider = new Slider(1, 5, 1, false, game.sliderStyle);
        slider.setValue(3);
        table.add(slider).minWidth(120).maxHeight(10).padTop(30).padLeft(20);
        table.row();
        temperatureSlider = slider;

        label = new Label(RimworldGame.bundle.get("humidity"), game.labelStyle);
        table.add(label).minWidth(120).height(50).padTop(30);
        slider = new Slider(1, 5, 1, false, game.sliderStyle);
        slider.setValue(3);
        table.add(slider).minWidth(120).maxHeight(10).padTop(30).padLeft(20);
        table.row();
        humiditySlider = slider;

        TextButton button = new TextButton(RimworldGame.bundle.get("back"), game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireBackEvent();
            }
        });
        table.add(button).minWidth(120).height(50).padTop(30);
        button = new TextButton(RimworldGame.bundle.get("next"), game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireNextEvent();
            }
        });
        table.add(button).minWidth(120).height(50).padTop(30).padLeft(20);

        stage.addActor(table);
        table.setFillParent(true);
        table.setVisible(true);
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private void fireBackEvent() {
        game.showScreen(PREVIOUS_SCREEN);
    }

    private void fireNextEvent() {
        int temperature = (int) temperatureSlider.getValue();
        int humidity = (int) humiditySlider.getValue();
        info.temperatureModifier = WorldInfo.getTemperatureModifier(temperature);
        info.humidityModifier = WorldInfo.getHumidityModifier(humidity);
        info.setSeed(seedField.getText());
        info.size = 250;
        RimworldGame.info = info;
        System.out.println(RimworldGame.info.seed);
        game.showScreen(NEXT_SCREEN);
    }

    private class Adapter extends InputAdapter {
        @Override
        public boolean keyUp(int keycode) {
            if (keycode == Input.Keys.ESCAPE) {
                fireBackEvent();
                return true;
            }
            return false;
        }
    }
}
