package com.rimworld.game.screens.newgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.rimworld.game.RimworldGame;
import com.rimworld.game.screens.StagedScreen;

public class ChooseScenario extends StagedScreen {
    private final static int PREVIOUS_SCREEN = RimworldGame.MAIN_MENU;
    private final static int NEXT_SCREEN = RimworldGame.CHOOSE_WORLD_SEED;

    public ChooseScenario(RimworldGame game) {
        super(game);

        createTable();
    }

    private void createTable() {
        Table table = new Table();
        TextButton button = new TextButton("Back", game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireBackEvent();
            }
        });
        table.add(button).width(100);

        button = new TextButton("Next", game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireNextEvent();
            }
        });
        table.add(button).width(100).padLeft(50);

        stage.addActor(table);
        table.setFillParent(true);
        table.setVisible(true);
    }

    @Override
    public void render(float delta) {
        handleInput();
        super.render(delta);
        act();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private void fireBackEvent() {
        game.showScreen(PREVIOUS_SCREEN);
    }

    private void fireNextEvent() {
        game.showScreen(NEXT_SCREEN);
    }

    private void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            fireBackEvent();
            return;
        }
    }
}
