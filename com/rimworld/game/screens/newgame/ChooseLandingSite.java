package com.rimworld.game.screens.newgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.rimworld.engine.map.global.GlobalMap;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.game.RimworldGame;
import com.rimworld.game.screens.StagedScreen;

import java.util.Random;

public class ChooseLandingSite extends StagedScreen {
    private final static int PREVIOUS_SCREEN = RimworldGame.CHOOSE_WORLD_SEED;
    private final static int NEXT_SCREEN = RimworldGame.GAME_SCREEN;

    public GlobalMap globalMap;

    private int chosenX;
    private int chosenY;
    private Batch batch;
    private Texture mouseoverTile;
    private Texture chosenTile;
    private Window window;

    public ChooseLandingSite(RimworldGame game) {
        super(game);
        batch = stage.getBatch();
        mouseoverTile = new Texture(Gdx.files.internal("textures/world/mousetile.png"));
        chosenTile = new Texture(Gdx.files.internal("textures/world/selectedtile.png"));

        Adapter adapter = new Adapter();
        multiplexer.addProcessor(adapter);
    }

    @Override
    public void initialize() {
        super.initialize();
        RimworldGame.info.randSeeder = new Random(RimworldGame.info.seed);

        globalMap = new GlobalMap(RimworldGame.info);

        camera.zoom = 1f;
        camera.setToOrtho(false);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();
        chosenX = -1;
        chosenY = -1;

        stage.clear();
        createTable();
    }

    private void createTable() {
        Table masterTable = new Table();

        masterTable.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.addActor(masterTable);
        masterTable.setFillParent(true);

        Table table = new Table();
        int buttonHeight = 50;
        int buttonWidth = 180;
        int windowHeight = Gdx.graphics.getHeight() / 2 + 200;
        int windowWidth = 450;

        TextButton button;
        button = new TextButton(RimworldGame.bundle.get("back"), game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireBackEvent();
            }
        });
        table.add(button).width(buttonWidth).height(buttonHeight);

        button = new TextButton(RimworldGame.bundle.get("next"), game.textButtonStyle);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fireNextEvent();
            }
        });
        table.add(button).width(buttonWidth).height(buttonHeight);

        window = new Window(RimworldGame.bundle.get("landscape"), game.windowStyle);
        window.setMovable(false);
        window.setResizable(false);
        window.setVisible(false);
        window.setModal(false);

        Table windowTable = new Table();
        windowTable.add(window).align(Align.left).prefHeight(windowHeight).prefWidth(windowWidth).left();
        windowTable.add(new Table()).prefWidth(Gdx.graphics.getWidth() - windowWidth);

        masterTable.add(windowTable).align(Align.left).prefHeight(Gdx.graphics.getHeight() - buttonHeight).prefWidth(Gdx.graphics.getWidth()).left();
        masterTable.row();

        masterTable.add(table).align(Align.bottom).prefHeight(buttonHeight).prefWidth(Gdx.graphics.getWidth());
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        handleInput();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        for (int i = 0; i < GlobalMap.WORLD_HEIGHT; i++) {
            for (int j = 0; j < GlobalMap.WORLD_WIDTH; j++) {
                Sprite s = globalMap.tiles[i][j].getBiome().sprite;
                s.setPosition(j * 5, i * 5);
                s.draw(batch);
            }
        }

        int x = Gdx.input.getX();
        int y = Gdx.input.getY();
        Vector3 v = camera.unproject(new Vector3(x, y, 0));
        x = (int) v.x;
        y = (int) v.y;
        x -= (x % 5);
        y -= (y % 5);
        batch.draw(mouseoverTile, x, y, 5, 5);

        if (chosenX != -1 && chosenY != -1) {
            batch.draw(chosenTile, chosenX * 5, chosenY * 5, 5, 5);
        }

        batch.end();
        act();
    }

    @Override
    public void dispose() {
        super.dispose();
        mouseoverTile.dispose();
        chosenTile.dispose();
    }

    private void fireBackEvent() {
        game.showScreen(PREVIOUS_SCREEN);
    }

    private void fireNextEvent() {
        game.chosenTile = globalMap.tiles[chosenY][chosenX];
        System.out.println(game.chosenTile.info.seed);
        game.chosenTile.info = RimworldGame.info;
        game.showScreen(NEXT_SCREEN);
    }

    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.zoom += 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom -= 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-3, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(3, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0, -3, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0, 3, 0);
        }
        camera.zoom = MathUtils.clamp(camera.zoom, 0.1f, 2000.0f / camera.viewportWidth);

        float effectiveViewportWidth = camera.viewportWidth * camera.zoom;
        float effectiveViewportHeight = camera.viewportHeight * camera.zoom;

        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth / 2f, 2000 - effectiveViewportWidth / 2f);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight / 2f, 2000 - effectiveViewportHeight / 2f);
        camera.update();
    }

    private void setWindowInfo(GlobalTile tile) {
        window.clear();
        window.left();
        window.setModal(false);
        Table table = new Table();

        Label label = new Label(RimworldGame.bundle.get((String) tile.getBiome().getProperty("biomeName").getStat()), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("id") + ": " + tile.getID(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("latitude") + ": " + tile.getLatitude(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("longitude") + ": " + tile.getLongitude(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("elevation") + ": " + tile.getElevation(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("temperature") + ": " + tile.getTemperature(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        label = new Label(RimworldGame.bundle.get("humidity") + ": " + tile.getHumidity(), game.labelStyle);
        table.add(label).align(Align.left).padLeft(20);
        table.add(new Table());
        table.row();

        window.add(table).prefWidth(window.getPrefWidth());
    }

    private class Adapter extends InputAdapter {
        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (button != Input.Buttons.LEFT) {
                window.setVisible(false);
                return false;
            }
            Vector3 v = camera.unproject(new Vector3(screenX, screenY, 0));
            chosenX = (int) v.x;
            chosenY = (int) v.y;
            chosenX /= 5;
            chosenY /= 5;
            window.setVisible(true);
            setWindowInfo(globalMap.tiles[chosenY][chosenX]);
            return true;
        }

        @Override
        public boolean keyUp(int keycode) {
            if (keycode == Input.Keys.ESCAPE) {
                fireBackEvent();
                return true;
            }
            return false;
        }
    }
}
