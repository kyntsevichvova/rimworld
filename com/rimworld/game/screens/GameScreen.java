package com.rimworld.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.rimworld.engine.generation.noise.Generator;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.local.LocalMap;
import com.rimworld.engine.map.local.TileGenerator;
import com.rimworld.engine.world.WorldInfo;
import com.rimworld.game.RimworldGame;

import java.util.Timer;
import java.util.TimerTask;

public class GameScreen extends StagedScreen {
    private final static int PREVIOUS_SCREEN = RimworldGame.CHOOSE_LANDING_SITE;

    private WorldInfo info;
    private GlobalTile tile;
    private LocalMap localMap;

    private TiledMap tiledMap;
    private Renderer renderer;
    private Sprite pawn;
    private float destx;
    private float desty;

    public GameScreen(RimworldGame game) {
        super(game);
        tiledMap = new TiledMap();

        Adapter adapter = new Adapter();
        multiplexer.addProcessor(adapter);
        pawn = new Sprite(new Texture(Gdx.files.internal("textures/iconhuman.png")));
    }

    @Override
    public void initialize() {
        pawn.setPosition(0, 0);
        pawn.setSize(64, 64);
        destx = 0;
        desty = 0;
        info = RimworldGame.info;
        tile = game.chosenTile;
        localMap = null;
        Gdx.graphics.setContinuousRendering(false);
        if (localMap == null) {
            localMap = new LocalMap(tile);
            TileGenerator.getTile(localMap, tile);
        }
        tiledMap = new TiledMap();
        TiledMapTileLayer layer = new TiledMapTileLayer(info.size, info.size, 64, 64);
        for (int i = 0; i < info.size; i++) {
            for (int j = 0; j < info.size; j++) {
                layer.setCell(i, j, new TiledMapTileLayer.Cell().setTile(
                        new StaticTiledMapTile(RimworldGame.terrainAtlas.findRegion(
                                (String) localMap.terrains[i][j].getProperty("region").getStat())
                        )
                ));
            }
        }
        Generator g = new Generator(info.randSeeder.nextInt(), 6, (1.0 / 0.005), (1.0 / 2.0), 0.5);
        MyLayer l = new MyLayer(info.size, info.size);
        for (int i = 0; i < info.size; i++) {
            for (int j = 0; j < info.size; j++) {
                if ((localMap.terrains[i][j].getProperty("name").getStat()).equals("Soil") && g.getValue(i, j) > 0.9) {
                    MyMapObject o = new MyMapObject();
                    Sprite s = new Sprite(new Texture(Gdx.files.internal("textures/treeoaka.png")));
                    s.setSize(60, 80);
                    s.setPosition(i * 64, j * 64);
                    l.addObject(i, j, o);
                    l.objects[i][j].sprite = s;
                }
            }
        }
        tiledMap.getLayers().add(layer);
        tiledMap.getLayers().add(l);
        camera.zoom = 1f;
        camera.setToOrtho(false);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (!(pawn.getX() == destx && pawn.getY() == desty)) {
                    float dx = destx - pawn.getX();
                    float dy = desty - pawn.getY();
                    float speed = 7;
                    dx = Math.min(dx, speed);
                    dx = Math.max(dx, -speed);
                    dy = Math.min(dy, speed);
                    dy = Math.max(dy, -speed);
                    pawn.setPosition(pawn.getX() + dx, pawn.getY() + dy);

                    camera.position.x = pawn.getX();
                    camera.position.y = pawn.getY();

                    float effectiveViewportWidth = camera.viewportWidth * camera.zoom;
                    float effectiveViewportHeight = camera.viewportHeight * camera.zoom;

                    camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth / 2f, 250f * 64 - effectiveViewportWidth / 2f);
                    camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight / 2f, 250f * 64 - effectiveViewportHeight / 2f);
                    camera.update();
                }
                Gdx.graphics.requestRendering();
            }
        }, 0, 16);

        renderer = new Renderer(tiledMap);
        Gdx.graphics.requestRendering();
    }

    @Override
    public void render(float delta) {
        handleInput();
        //super.render(delta);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.setView(camera);
        renderer.render();
        renderer.getBatch().begin();
        pawn.draw(renderer.getBatch());
        renderer.getBatch().end();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.zoom += 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom -= 0.02;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-8, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(8, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0, -8, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0, 8, 0);
        }
        camera.zoom = MathUtils.clamp(camera.zoom, 0.1f, 250f * 64 / camera.viewportWidth);

        float effectiveViewportWidth = camera.viewportWidth * camera.zoom;
        float effectiveViewportHeight = camera.viewportHeight * camera.zoom;

        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth / 2f, 250f * 64 - effectiveViewportWidth / 2f);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight / 2f, 250f * 64 - effectiveViewportHeight / 2f);
        camera.update();
    }


    private class Adapter extends InputAdapter {
        @Override
        public boolean keyUp(int keycode) {
            if (keycode == Input.Keys.ESCAPE) {
                fireBackEvent();
                return true;
            }
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (button == Input.Buttons.RIGHT) {
                Vector3 v = new Vector3(screenX, screenY, 0);
                v = camera.unproject(v);
                //v = camera.project(v);
                destx = v.x;
                desty = v.y;
            }
            return true;
        }
    }

    private void fireBackEvent() {
        game.showScreen(PREVIOUS_SCREEN);
    }

    private class Renderer extends OrthogonalTiledMapRenderer {
        public Renderer(TiledMap map) {
            super(map);
        }

        public void renderObjects(MapLayer layer) {
            MyLayer l = (MyLayer) (layer);
            this.batch.setProjectionMatrix(camera.combined);
            for (int i = 0; i < l.objects.length; i++) {
                for (int j = 0; j < l.objects[i].length; j++) {
                    if (l.objects[i][j].sprite != null) {
                        l.objects[i][j].sprite.draw(this.batch);
                    }
                }
            }
        }
    }

    private class MyLayer extends MapLayer {
        public MyMapObject[][] objects;

        public MyLayer(int height, int width) {
            objects = new MyMapObject[height][width];
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    objects[i][j] = new MyMapObject();
                }
            }
        }

        public void addObject(int x, int y, MyMapObject o) {
            objects[x][y] = o;
        }
    }

    private class MyMapObject {
        public Sprite sprite;

        public MyMapObject() {

        }
    }
}
