package com.rimworld.game;

import com.badlogic.gdx.Gdx;

import java.util.TimerTask;

public class Ticker extends TimerTask {
    private int tick;
    private boolean paused;

    public Ticker(int tick) {
        this.tick = tick;
        this.paused = true;
    }

    @Override
    public void run() {
        emulateTick();
    }

    private void emulateTick() {
        if (paused) {

            return;
        }
        tick++;

        Gdx.graphics.requestRendering();
    }
}
