package com.rimworld.engine;

import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertyHolder {
    private Map<String, Property<?>> properties;

    public PropertyHolder() {
        properties = new HashMap<>();
    }

    public PropertyHolder(File file) {
        try {
            List<String> list = Files.readAllLines(file.toPath());
            this.properties = holderFromStrings(list).properties;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Property<?> getProperty(String name) {
        return properties.get(name);
    }

    public boolean hasPropety(String name) {
        return properties.containsKey(name);
    }

    public void addProperty(Property<?> property) {
        properties.put(property.getName().getInternalName(), property);
    }

    public static PropertyHolder holderFromStrings(List<String> list) {
        PropertyHolder holder = new PropertyHolder();
        for (String s : list) {
            s = s.trim();
            String p = "", type = "", value = "";
            StringBuilder buf = new StringBuilder();
            int stage = 0;
            for (Character c : s.toCharArray()) {
                if (c.equals('<')) {
                    if (stage == 2) {
                        value = buf.toString();
                        break;
                    }
                }
                buf.append(c);
                if (c.equals('>')) {
                    if (stage == 0) {
                        p = buf.toString();
                    } else if (stage == 1) {
                        type = buf.toString();
                    }
                    buf = new StringBuilder();
                    stage++;
                }
            }
            p = p.substring(1, p.length() - 1);
            type = type.substring(1, type.length() - 1);
            Property<?> property;
            switch (type) {
                case "string": property = new Property<>(new NameString(p), value); break;
                case "double": property = new Property<>(new NameString(p), Double.valueOf(value)); break;
                case "int": property = new Property<>(new NameString(p), Integer.valueOf(value)); break;
                case "bool": property = new Property<>(new NameString(p), Boolean.valueOf(value)); break;
                default: property = new Property<>(new NameString(""), value);
            }
            holder.addProperty(property);
        }
        return holder;
    }
}
