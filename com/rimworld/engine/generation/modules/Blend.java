package com.rimworld.engine.generation.modules;

import com.rimworld.engine.generation.Utils;

public class Blend extends ModuleBase {
    public Blend() {
        super(3);
    }

    public Blend(ModuleBase lhs, ModuleBase rhs, ModuleBase controller) {
        this();
        this.modules[0] = lhs;
        this.modules[1] = rhs;
        this.modules[2] = controller;
    }

    @Override
    public double getValue(double x, double y) {
        double value = this.modules[0].getValue(x, y);
        double value2 = this.modules[1].getValue(x, y);
        double position = (this.modules[2].getValue(x, y) + 1.0) / 2.0;
        return Utils.InterpolateLinear(value, value2, position);
    }
}
