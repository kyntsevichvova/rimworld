package com.rimworld.engine.generation.modules;

public class Power extends ModuleBase {

    public Power() {
        super(2);
    }

    public Power(ModuleBase lhs, ModuleBase pow) {
        this();
        this.modules[0] = lhs;
        this.modules[1] = pow;
    }

    @Override
    public double getValue(double x, double y) {
        return Math.pow(this.modules[0].getValue(x, y), this.modules[1].getValue(x, y));
    }
}
