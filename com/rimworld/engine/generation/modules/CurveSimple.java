package com.rimworld.engine.generation.modules;

public class CurveSimple extends ModuleBase {
    public CurveSimple() {
        super(2);
    }

    public CurveSimple(ModuleBase lhs, ModuleBase rhs) {
        this();
        modules[0] = lhs;
        modules[1] = rhs;
    }

    @Override
    public double getValue(double x, double y) {
        return modules[1].getValue(modules[0].getValue(x, y), y);
    }
}
