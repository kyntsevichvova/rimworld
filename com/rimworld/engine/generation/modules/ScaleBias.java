package com.rimworld.engine.generation.modules;

public class ScaleBias extends ModuleBase {
    private double scale = 1.0;

    private double bias;

    public ScaleBias() {
        super(1);
    }

    public ScaleBias(ModuleBase input) {
        this();
        this.modules[0] = input;
    }

    public ScaleBias(double scale, double bias, ModuleBase input) {
        this(input);
        this.bias = bias;
        this.scale = scale;
    }

    @Override
    public double getValue(double x, double y) {
        return this.modules[0].getValue(x, y) * this.scale + this.bias;
    }
}
