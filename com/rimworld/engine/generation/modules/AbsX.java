package com.rimworld.engine.generation.modules;

public class AbsX extends ModuleBase {
    public AbsX() {
        super(1);
    }

    public AbsX(ModuleBase module) {
        this();
        this.modules[0] = module;
    }

    @Override
    public double getValue(double x, double y) {
        return modules[0].getValue(Math.abs(x), y);
    }
}
