package com.rimworld.engine.generation.modules;

public class CurvePoint {
    private double x;
    private double y;

    public CurvePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
