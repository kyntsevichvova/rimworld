package com.rimworld.engine.generation.modules;

import com.rimworld.engine.generation.Utils;

import java.util.ArrayList;
import java.util.List;

public class Curve extends ModuleBase {
    private List<CurvePoint> points;

    public Curve(int cnt) {
        super(0);
        points = new ArrayList<>();
    }

    public Curve(List<CurvePoint> list, int cnt) {
        this(cnt);
        points = list;
    }

    public Curve(List<CurvePoint> list) {
        this(list, list.size());
    }

    @Override
    public double getValue(double x, double y) {
        if (x < points.get(0).getX()) {
            return Utils.InterpolateLinear(x, points.get(0).getY(), 0);
        }
        for (int i = 0; i < points.size(); i++) {
            if (i + 1 >= points.size() || (points.get(i).getX() <= x && x <= points.get(i + 1).getX())) {
                double a = points.get(i).getY();
                double b;
                double position;
                if (i + 1 >= points.size()) {
                    b = x;
                    position = 1;
                } else {
                    b = points.get(i + 1).getY();
                    position = (x - points.get(i).getX()) / (points.get(i + 1).getX() - points.get(i).getX());
                }
                return Utils.InterpolateLinear(a, b, position);
            }
        }
        return 0;
    }
}
