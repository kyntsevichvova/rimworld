package com.rimworld.engine.generation.modules;

public class RainfallProcessor extends ModuleBase {

    public RainfallProcessor() {
        super(1);
    }

    public RainfallProcessor(ModuleBase input) {
        this();
        this.modules[0] = input;
    }

    @Override
    public double getValue(double x, double y) {
        double val = modules[0].getValue(x, y);
        if (val < 0.0) {
            val = 0.0;
        }
        if (val < 0.120) {
            val = (val + 0.120) / 2.0;
            if (val < 0.03) {
                val = (val + 0.03) / 2.0;
            }
        }
        return val;
    }
}
