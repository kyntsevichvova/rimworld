package com.rimworld.engine.generation.modules;

public abstract class ModuleBase {
    protected ModuleBase[] modules;

    public ModuleBase(int count) {
        if (count > 0) {
            this.modules = new ModuleBase[count];
        }
    }

    public abstract double getValue(double x, double y);

    public ModuleBase getModule(int num) {
        if (num < 0 || num >= this.modules.length) {
            throw new IndexOutOfBoundsException();
        }
        return this.modules[num];
    }

    public void setModule(int num, ModuleBase value) {
        if (num < 0 || num >= this.modules.length) {
            throw new IndexOutOfBoundsException();
        }
        this.modules[num] = value;
    }
}
