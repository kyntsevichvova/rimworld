package com.rimworld.engine.generation.modules;

public class AbsY extends ModuleBase {
    public AbsY() {
        super(1);
    }

    public AbsY(ModuleBase module) {
        this();
        this.modules[0] = module;
    }

    @Override
    public double getValue(double x, double y) {
        return modules[0].getValue(x, Math.abs(y));
    }
}
