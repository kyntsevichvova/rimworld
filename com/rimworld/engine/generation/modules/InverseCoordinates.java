package com.rimworld.engine.generation.modules;

public class InverseCoordinates extends ModuleBase {
    public InverseCoordinates() {
        super(1);
    }

    public InverseCoordinates(ModuleBase module) {
        this();
        this.modules[0] = module;
    }

    @Override
    public double getValue(double x, double y) {
        return modules[0].getValue(y, x);
    }
}
