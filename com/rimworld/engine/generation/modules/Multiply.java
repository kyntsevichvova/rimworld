package com.rimworld.engine.generation.modules;

public class Multiply extends ModuleBase {

    public Multiply() {
        super(2);
    }

    public Multiply(ModuleBase lhs, ModuleBase rhs) {
        this();
        this.modules[0] = lhs;
        this.modules[1] = rhs;
    }

    @Override
    public double getValue(double x, double y) {
        return this.modules[0].getValue(x, y) * this.modules[1].getValue(x, y);
    }
}
