package com.rimworld.engine.generation.modules;

public class Clamp extends ModuleBase {
    private double min = -1.0;
    private double max = 1.0;

    public Clamp() {
        super(1);
    }

    public Clamp(ModuleBase input) {
        this();
        this.modules[0] = input;
    }

    public Clamp(double min, double max, ModuleBase input) {
        this();
        this.min = min;
        this.max = max;
        this.modules[0] = input;
    }

    public void SetBounds(double min, double max) {
        this.min = min;
        this.max = max;
    }

    public double getValue(double x, double y) {
        if (this.min > this.max) {
            double min = this.min;
            this.min = this.max;
            this.max = min;
        }
        double value = this.modules[0].getValue(x, y);
        if (value < this.min) {
            return this.min;
        }
        if (value > this.max) {
            return this.max;
        }
        return value;
    }
}
