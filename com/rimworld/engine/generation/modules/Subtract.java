package com.rimworld.engine.generation.modules;

public class Subtract extends ModuleBase {
    public Subtract() {
        super(2);
    }

    public Subtract(ModuleBase lhs, ModuleBase rhs) {
        this();
        this.setModule(0, lhs);
        this.setModule(1, rhs);
    }

    @Override
    public double getValue(double x, double y) {
        return this.modules[0].getValue(x, y) - this.modules[1].getValue(x, y);
    }
}
