package com.rimworld.engine.generation.modules;

public class Const extends ModuleBase {
    private double value;

    public Const() {
        super(0);
    }

    public Const(double value) {
        this();
        this.value = value;
    }

    @Override
    public double getValue(double x, double y) {
        return value;
    }
}
