package com.rimworld.engine.generation.modules;

public class Add extends ModuleBase {
    public Add() {
        super(2);
    }

    public Add(ModuleBase lhs, ModuleBase rhs) {
        this();
        this.modules[0] = lhs;
        this.modules[1] = rhs;
    }

    @Override
    public double getValue(double x, double y) {
        return this.modules[0].getValue(x, y) + this.modules[1].getValue(x, y);
    }
}
