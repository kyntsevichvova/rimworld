package com.rimworld.engine.generation;

public class Utils {
    public static double InterpolateLinear(double a, double b, double position) {
        return (1.0 - position) * a + position * b;
    }

    public static double InterpolateCubic(double a, double b, double c, double d, double position) {
        double num = d - c - (a - b);
        double num2 = a - b - num;
        double num3 = c - a;
        return num * position * position * position + num2 * position * position + num3 * position + b;
    }
}
