package com.rimworld.engine.generation.th;

public enum OverallTemperature {
    VeryCold,
    Cold,
    Normal,
    Hot,
    VeryHot
}
