package com.rimworld.engine.generation.th;

import com.rimworld.engine.generation.modules.Curve;
import com.rimworld.engine.generation.modules.CurvePoint;
import com.rimworld.engine.generation.modules.ModuleBase;

import java.util.Arrays;

public class OverallRainfallUtility {
    private static boolean curvesInitialized = false;
    private static ModuleBase almostNoneCurve;
    private static ModuleBase littleCurve;
    private static ModuleBase highCurve;
    private static ModuleBase veryHighCurve;

    private static void initialize() {
        curvesInitialized = true;
        almostNoneCurve = new Curve(Arrays.asList(
                new CurvePoint(0, 0),
                new CurvePoint(1500, 120),
                new CurvePoint(3500, 180),
                new CurvePoint(6000, 200),
                new CurvePoint(12000, 250)
        ));
        littleCurve = new Curve(Arrays.asList(
                new CurvePoint(0, 0),
                new CurvePoint(1500, 300),
                new CurvePoint(6000, 1100),
                new CurvePoint(12000, 1400)
        ));
        highCurve = new Curve(Arrays.asList(
                new CurvePoint(0, 500),
                new CurvePoint(150, 950),
                new CurvePoint(500, 2000),
                new CurvePoint(1000, 2800),
                new CurvePoint(5000, 6000),
                new CurvePoint(12000, 12000)
        ));
        veryHighCurve = new Curve(Arrays.asList(
                new CurvePoint(0, 750),
                new CurvePoint(125, 2000),
                new CurvePoint(500, 3000),
                new CurvePoint(1000, 3800),
                new CurvePoint(5000, 7500),
                new CurvePoint(12000, 12000)
        ));
    }

    public static ModuleBase getCurve(OverallRainfall modifier) {
        if (!curvesInitialized) {
            initialize();
        }
        switch (modifier) {
            case AlmostNone:
                return almostNoneCurve;
            case Little:
                return littleCurve;
            case High:
                return highCurve;
            case VeryHigh:
                return veryHighCurve;
        }
        return null;
    }
}
