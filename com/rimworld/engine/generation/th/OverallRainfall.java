package com.rimworld.engine.generation.th;

public enum OverallRainfall {
    AlmostNone,
    Little,
    Normal,
    High,
    VeryHigh
}
