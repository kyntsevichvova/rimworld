package com.rimworld.engine.generation.th;

import com.rimworld.engine.generation.modules.Curve;
import com.rimworld.engine.generation.modules.CurvePoint;
import com.rimworld.engine.generation.modules.ModuleBase;

import java.util.Arrays;

public class OverallTemperatureUtility {
    private static boolean curvesInitialized = false;
    private static ModuleBase veryColdCurve;
    private static ModuleBase coldCurve;
    private static ModuleBase hotCurve;
    private static ModuleBase veryHotCurve;

    private static void initialize() {
        curvesInitialized = true;
        veryColdCurve = new Curve(Arrays.asList(
            new CurvePoint(-50, -75),
            new CurvePoint(-40, -60),
            new CurvePoint(0, -35),
            new CurvePoint(20, -28),
            new CurvePoint(25, -18),
            new CurvePoint(30, -8.5),
            new CurvePoint(50, -7)
        ));
        coldCurve = new Curve(Arrays.asList(
                new CurvePoint(-50, -70),
                new CurvePoint(-25, -40),
                new CurvePoint(-20, -25),
                new CurvePoint(-13, -15),
                new CurvePoint(0, -12),
                new CurvePoint(30, -3),
                new CurvePoint(60, 25)
        ));
        hotCurve = new Curve(Arrays.asList(
                new CurvePoint(-45, -22),
                new CurvePoint(-25, -12),
                new CurvePoint(-22, 2),
                new CurvePoint(-10, 25),
                new CurvePoint(40, 57),
                new CurvePoint(120, 120)
        ));
        veryHotCurve = new Curve(Arrays.asList(
                new CurvePoint(-45, 25),
                new CurvePoint(0, 40),
                new CurvePoint(33, 70),
                new CurvePoint(40, 75),
                new CurvePoint(120, 120)
        ));
    }

    public static ModuleBase getCurve(OverallTemperature modifier) {
        if (!curvesInitialized) {
            initialize();
        }
        switch (modifier) {
            case VeryCold:
                return veryColdCurve;
            case Cold:
                return coldCurve;
            case Hot:
                return hotCurve;
            case VeryHot:
                return veryHotCurve;
        }
        return null;
    }
}
