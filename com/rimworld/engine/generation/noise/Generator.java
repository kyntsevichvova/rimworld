package com.rimworld.engine.generation.noise;

import com.rimworld.engine.generation.modules.ModuleBase;

import java.util.Random;

public class Generator extends ModuleBase {
    private int octavesNumber;
    private SimplexNoise2D[] octaves;
    private double[] frequencies;
    private double[] amplitudes;

    public Generator() {
        super(0);
    }

    public Generator(int seed, int octavesNumber, double baseFrequency, double frequency, double persistence) {
        this();
        this.octavesNumber = octavesNumber;
        octaves = new SimplexNoise2D[octavesNumber];
        frequencies = new double[octavesNumber];
        amplitudes = new double[octavesNumber];

        Random rand = new Random(seed);

        for (int i = 0; i < octavesNumber; i++) {
            octaves[i] = new SimplexNoise2D(rand.nextInt());
            frequencies[i] = (i > 0 ? frequencies[i - 1] * frequency : baseFrequency);
            amplitudes[i] = (i > 0 ? amplitudes[i - 1] * persistence : 1);
        }
    }

    @Override
    public double getValue(double x, double y) {
        double noise = 0;
        for (int i = 0; i < octavesNumber; i++) {
            double temp = octaves[i].noise(x / frequencies[i], y / frequencies[i]);
            noise += amplitudes[i] * temp;
        }
        return noise;
    }
}
