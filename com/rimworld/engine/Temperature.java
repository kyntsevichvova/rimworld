package com.rimworld.engine;

public class Temperature {
    private double degree;

    public Temperature(double degree) {
        this.degree = degree;
    }

    public double getDegree() {
        return degree;
    }

    public void setDegree(double newDegree) throws IllegalArgumentException {
        if (!(-270 <= newDegree && newDegree <= 1000)) {
            throw new IllegalArgumentException("Temperature should be in [-270..1000]");
        }
        this.degree = newDegree;
    }

    public double difference(Temperature to) {
        return degree - to.getDegree();
    }

    @Override
    public String toString() {
        return String.valueOf(Math.round(degree * 10) / 10);
    }
}
