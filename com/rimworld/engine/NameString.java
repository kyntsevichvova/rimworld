package com.rimworld.engine;

public class NameString {
    private final String internalName;
    private final String displayedName;

    public NameString(String internalName, String displayedName) {
        this.internalName = internalName;
        this.displayedName = displayedName;
    }

    public NameString(String internalName) {
        this(internalName, internalName);
    }

    public String getInternalName() {
        return internalName;
    }

    public String getDisplayedName() {
        return displayedName;
    }
}
