package com.rimworld.engine.entity.statics.structures;

import com.rimworld.engine.Properties;
import com.rimworld.engine.entity.statics.StaticEntity;
import com.rimworld.engine.map.local.Area;
import com.rimworld.engine.map.local.Point;

public class Furniture extends Building {
    private boolean meetingAllowed;
    private boolean forMedical;
    private boolean forPrisoners;
    //private List<Pawn> owner;
    //private Point usagePoint;

    public boolean isSleepingAllowed() {
        return hasPropety(Properties.SLEEPING_SPOT);
    }

    public boolean isSittingAllowed() {
        return hasPropety(Properties.SITTING_SPOT);
    }

    public boolean isMeetingPoint() {
        return hasPropety(Properties.MEETING_SPOT);
    }

    public boolean isMeetingAllowed() {
        return meetingAllowed;
    }

    public void setMeetingAllowed(boolean meetingAllowed) {
        this.meetingAllowed = meetingAllowed;
    }

    public boolean isForMedical() {
        return forMedical;
    }

    public void setForMedical(boolean forMedical) {
        this.forMedical = forMedical;
    }

    public boolean isForPrisoners() {
        return forPrisoners;
    }

    public void setForPrisoners(boolean forPrisoners, boolean changeRoom) throws IllegalArgumentException {
        if (changeRoom) {
            Area area = map.getEnclosedArea(points.get(0));
            if (area == null) {
                throw new IllegalStateException("Can't set unenclosed prisoner bed");
            } else {
                for (Point p : area.getPoints()) {
                    for (StaticEntity entity : map.getTile(p).getStaticEntities()) {
                        if (entity instanceof Furniture) {
                            Furniture furniture = (Furniture) entity;
                            if (furniture.isSleepingAllowed()) {
                                furniture.setForPrisoners(forPrisoners, false);
                            }
                        }
                    }
                }
            }
        } else {
            this.forPrisoners = forPrisoners;
        }
    }
}
