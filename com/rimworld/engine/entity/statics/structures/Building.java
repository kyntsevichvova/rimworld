package com.rimworld.engine.entity.statics.structures;

import com.rimworld.engine.Properties;
import com.rimworld.engine.Property;

public abstract class Building extends Structure {
    protected int pawnsUsing;

    public int getMaxPawnsUsing() {
        return ((Property<Integer>) getProperty(Properties.MAX_PAWNS_USING)).getStat();
    }

    public int getPawnsUsing() {
        return pawnsUsing;
    }

    public void setPawnsUsing(int pawnsUsing) throws IllegalArgumentException {
        if (!(0 <= pawnsUsing && pawnsUsing <= getMaxPawnsUsing())) {
            throw new IllegalArgumentException("Pawns using should be in [0..maxPawnsUsing]");
        }
        this.pawnsUsing = pawnsUsing;
    }
}
