package com.rimworld.engine.entity.statics.structures;

import com.rimworld.engine.entity.statics.StaticEntity;
import com.rimworld.engine.map.local.Point;

import java.util.List;

public abstract class Structure extends StaticEntity {
    //private Material basicMaterial;
    //private Faction faction;
    protected boolean claimable;
    protected boolean mineable;
    protected boolean deconstructable;
    protected boolean uninstallable;
    protected List<Point> points;

    /*
    public Material getBasicMaterial() {
        return this.basicMaterial;
    }
     */

    public boolean isClaimable() {
        return this.claimable;
    }

    public void setClaimable(boolean claimable) {
        this.claimable = claimable;
    }

    public boolean isMineable() {
        return this.mineable;
    }

    public boolean isDeconstructable() {
        return this.deconstructable;
    }

    public boolean isUninstallable() {
        return this.uninstallable;
    }
}
