package com.rimworld.engine.entity.statics.plants;

import com.rimworld.engine.Properties;
import com.rimworld.engine.Property;
import com.rimworld.engine.Temperature;
import com.rimworld.engine.entity.statics.StaticEntity;
import com.rimworld.engine.map.local.LocalMap;
import com.rimworld.engine.map.local.Point;
import com.rimworld.engine.world.World;

/*
Properties:
    nutrition, beauty modifier, grow days, lifespan,
    grow light min, grow light optimal, growth rate fertility factor
 */

public abstract class Plant extends StaticEntity {
    private final static Temperature GROW_COMFORTABLE_MIN = new Temperature(10);
    private final static Temperature GROW_COMFORTABLE_MAX = new Temperature(42);
    private final static Temperature GROW_POSSIBLE_MAX = new Temperature(58);

    private String plantName;
    private Point position;
    private LocalMap map;

    private double growthCurrent;
    private double growDays;
    private double lifespan;

    private double growthRateFertilityFactor;
    private int growLightMin;
    private int growLightOptimal;

    public Plant() {
        growthCurrent = 0.05;
    }

    protected double getGrowthRateFactorFertility() {
        return 1.0 + growthRateFertilityFactor *
                (((Property<Double>)map
                        .getTile(position)
                        .getSurface()
                        .getProperty(Properties.FERTILITY)).getStat() - 1
                );
    }

    protected double getGrowthRateFactorTemperature() {
        Temperature t = map.getTemperature(position);
        double rate;
        if (t.difference(GROW_COMFORTABLE_MIN) < 0) {
            rate = 1.0 * t.getDegree() / 100;
        } else if (t.difference(GROW_COMFORTABLE_MAX) <= 0) {
            rate = 1.0;
        } else {
            rate = (GROW_POSSIBLE_MAX.difference(t) / 150.0);
        }
        return rate;
    }

    protected double getGrowthRateFactorLight() {
        int l = map.getLight(position);
        return 1.0 * (l - growLightMin) / (growLightOptimal - growLightMin);
    }

    public double getGrowthRemaining() {
        return 1.0 - growthCurrent;
    }

    public double getGrowthPerTick() {
        return (getGrowthRemaining() * World.TICKS_PER_DAY * growDays) /
                (getGrowthRateFactorFertility()
                        * getGrowthRateFactorLight()
                        * getGrowthRateFactorTemperature());
    }
}
