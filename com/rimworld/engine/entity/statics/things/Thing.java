package com.rimworld.engine.entity.statics.things;

import com.rimworld.engine.entity.statics.StaticEntity;

public class Thing extends StaticEntity {
    private ThingDef def;

    public ThingDef getDef() {
        return def;
    }
}
