package com.rimworld.engine.entity.statics.things;

import com.rimworld.engine.NameString;
import com.rimworld.engine.PropertyHolder;

public class ThingDef extends PropertyHolder {
    private NameString name;

    public NameString getName() {
        return name;
    }
}
