package com.rimworld.engine.entity.dynamics;

import com.rimworld.engine.NameString;
import com.rimworld.engine.PropertyHolder;

import java.util.List;

public class Race {
    private NameString name;
    private String raceDescription;
    private int averageLifespan;
    private List<Gender> genders;
    /*Add consumable food types*/

    public Race(NameString name, String raceDescription) {
        this.name = name;
        this.raceDescription = raceDescription;
    }

    public Race(NameString name) {
        this(name, "Unknown");
    }

    public Race() {
        this(new NameString("unknown"));
    }

    public void setAverageLifespan(int averageLifespan) {
        this.averageLifespan = averageLifespan;
    }

    public int getAverageLifespan() {
        return averageLifespan;
    }

    public NameString getName() {
        return name;
    }

    public String getRaceDescription() {
        return this.raceDescription;
    }

    public class Gender {
        private NameString genderName;
        private List<Lifestage> lifestages;
    }

    public class Lifestage extends PropertyHolder {
        // body size, walk speed
    }
}
