package com.rimworld.engine.entity.dynamics.health;

import com.rimworld.engine.NameString;
import com.rimworld.engine.Property;
import com.rimworld.engine.PropertyHolder;

import java.util.List;

public class BodyPart extends PropertyHolder {
    private NameString name;
    private Property health;
    private BodyPart parent;
    private List<BodyPart> inside;
    private List<BodyPart> connected;
}
