package com.rimworld.engine.entity.dynamics.health;

import java.util.Set;

public class HealthCondition {
    private Set<BodyPart> bodyParts;
    private Set<MedicalCondition> medicalConditions;

    public Set<BodyPart> getBodyParts() {
        return bodyParts;
    }

    public Set<MedicalCondition> getMedicalConditions() {
        return medicalConditions;
    }

    public void updateBodyPart(BodyPart oldPart, BodyPart newPart) {
        bodyParts.remove(oldPart);
        bodyParts.add(newPart);
    }

    public void addMedicalCondition(MedicalCondition condition) {
        /*
        Add medical condition
         */
    }
}
