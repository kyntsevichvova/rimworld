package com.rimworld.engine.entity.dynamics;

import com.rimworld.engine.PropertyHolder;
import com.rimworld.engine.entity.dynamics.health.HealthCondition;
import com.rimworld.engine.entity.dynamics.needs.Need;
import com.rimworld.engine.entity.dynamics.social.SocialInteraction;

import java.util.Map;

public abstract class DynamicEntity extends PropertyHolder {
    private Race race;
    private HealthCondition healthCondition;
    private SocialInteraction socialInteraction;
    private Map<String, Need> needs;

    public HealthCondition getHealthCondition() {
        return healthCondition;
    }

    public SocialInteraction getSocialInteraction() {
        return socialInteraction;
    }

    public Need getNeed(String needName) {
        return needs.get(needName);
    }

    public boolean hasNeed(String needName) {
        return needs.containsKey(needName);
    }

    public void addNeed(Need need) {
        needs.put(need.getName().getInternalName(), need);
    }
}
