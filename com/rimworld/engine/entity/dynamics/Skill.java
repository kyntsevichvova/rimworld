package com.rimworld.engine.entity.dynamics;

import com.rimworld.engine.NameString;

import java.util.Arrays;
import java.util.List;

public class Skill {
    private final static List<Integer> levelThresholds = Arrays.asList(
            1_000,
            2_000, 3_000, 4_000, 5_000, 6_000,
            7_000, 8_000, 9_000, 1_0000, 12_000,
            14_000, 16_000, 18_000, 20_000, 22_000,
            24_000, 26_000, 28_000, 30_000, 32_000
    );
    private NameString name;
    private double passionModifier;
    private int level;
    private double experience;
    private double dailyGained;
}
