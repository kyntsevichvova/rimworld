package com.rimworld.engine;

import com.rimworld.engine.entity.dynamics.Skill;
import com.rimworld.engine.entity.statics.things.ThingStack;
import javafx.util.Pair;

import java.util.List;

public class Bill {
    public enum BillType {
        NumberTimes, Until, Forever
    }

    private NameString name;

    private int workload;
    private List<Pair<Skill, Integer>> skillRequirements;
    private float basicFailureChance;
    /*
    Gain on success and gain on failure
     */
    private List<ThingStack> resources;

    private Permission permission;

    private int searchRadius;
    private int searchStockpile;
    private int dropStockpile;

    private BillType billType;
}
