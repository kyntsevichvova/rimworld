package com.rimworld.engine.map.local;

import com.rimworld.engine.Properties;
import com.rimworld.engine.Property;
import com.rimworld.engine.entity.statics.StaticEntity;

import java.util.Set;

public class LocalTile {
    private Surface surface;
    private Set<StaticEntity> staticEntities;

    public Surface getSurface() {
        return surface;
    }

    public Set<StaticEntity> getStaticEntities() {
        return staticEntities;
    }

    public boolean isPassable() {
        boolean passable = true;
        for (StaticEntity entity : staticEntities) {
            if (((Property<Double>)entity.getProperty(Properties.WALKABLE_SPEED)).getStat() == 0) {
                passable = false;
            }
        }
        return passable;
    }
}
