package com.rimworld.engine.map.local.pathfinding;

public class DSU {
    private int[] ancestor;
    private int[] rang;

    public DSU(int nodes) {
        ancestor = new int[nodes];
        rang = new int[nodes];
        for (int i = 0; i < nodes; i++) {
            ancestor[i] = i;
            rang[i] = 1;
        }
    }

    public boolean unite(int x, int y) {
        x = get(x);
        y = get(y);
        if (same(x, y)) {
            return false;
        }
        if (rang[x] < rang[y]) {
            int t = x;
            x = y;
            y = t;
        }
        rang[x] += rang[y];
        ancestor[y] = x;
        return true;
    }

    public boolean same(int x, int y) {
        return get(x) == get(y);
    }

    private int get(int v) {
        if (ancestor[v] == v) {
            return v;
        }
        ancestor[v] = get(ancestor[v]);
        return ancestor[v];
    }
}
