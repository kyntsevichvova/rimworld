package com.rimworld.engine.map.local;

import com.rimworld.engine.generation.modules.Const;
import com.rimworld.engine.generation.modules.ModuleBase;
import com.rimworld.engine.generation.modules.Multiply;
import com.rimworld.engine.generation.modules.ScaleBias;
import com.rimworld.engine.generation.noise.Generator;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.local.terrain.Terrain;
import com.rimworld.engine.map.local.terrain.TerrainThreshold;
import com.rimworld.engine.map.local.terrain.Terrains;
import com.rimworld.engine.map.local.terrain.rocks.RockNoises;
import com.rimworld.engine.map.local.terrain.rocks.RocksFromGrid;
import com.rimworld.engine.world.WorldInfo;
import com.rimworld.game.RimworldGame;

import java.util.Random;

public class TileGenerator {
    private static Random randSeeder;
    private static LocalMap map;
    private static WorldInfo info;
    private static GlobalTile tile;

    public static void getTile(LocalMap map0, GlobalTile tile0) {
        info = RimworldGame.info;
        System.out.println(info.seed);
        map = map0;
        tile = tile0;
        randSeeder = new Random(info.randSeeder.nextInt() + 2 * tile.getID());
        generateElevationFertility();
        generateTerrain();
    }

    private static void generateElevationFertility() {
        ModuleBase moduleBase = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.020999999716877937), (1.0 / 2.0), 0.5);
        moduleBase = new ScaleBias(0.5, 0.5, moduleBase);
        moduleBase = new Multiply(moduleBase, new Const(1.0));
        float b = (!tile.isWaterCovered()) ? Float.MAX_VALUE : 0;
        double[][] elevation = new double[info.size][info.size];
        for (int i = 0; i < info.size; i++) {
            for (int j = 0; j < info.size; j++) {
                elevation[i][j] = Math.min(moduleBase.getValue(i, j), b);
            }
        }
        ModuleBase moduleBase2 = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.020999999716877937), (1.0 / 2.0), 0.5);
        moduleBase2 = new ScaleBias(1.0, 0, moduleBase2);
        double[][] fertility = new double[info.size][info.size];
        for (int i = 0; i < info.size; i++) {
            for (int j = 0; j < info.size; j++) {
                fertility[i][j] = moduleBase2.getValue(i, j);
            }
        }
        map.elevation = elevation;
        map.fertility = fertility;
    }

    private static void generateTerrain() {
        RockNoises.init(randSeeder.nextInt());
        double[][] elevation = map.elevation;
        double[][] fertility = map.fertility;
        Terrain[][] terrains = new Terrain[info.size][info.size];
        map.terrains = terrains;
        ModuleBase density = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.015), (1.0 / 2.0), 0.5);
        for (int i = 0; i < info.size; i++) {
            for (int j = 0; j < info.size; j++) {
                boolean preferSolid = density.getValue(i, j) > 0.7;
                Terrain terrain = terrainFrom(i, j, elevation[i][j], fertility[i][j], preferSolid);
                terrains[i][j] = terrain;
            }
        }
    }

    private static Terrain terrainFrom(int x, int y, double elev, double fert, boolean preferSolid) {
        if (preferSolid) {
            return Terrains.getByName((String) RocksFromGrid.rockDefAt(x, y).getProperty("naturalTerrain").getStat());
        }
        Terrain terrain2;
        for (int i = 0; i < tile.biome.terrainPatchMakers.size(); i++) {
            terrain2 = tile.biome.terrainPatchMakers.get(i).terrainAt(x, y, tile, fert);
            if (terrain2 != null) {
                return terrain2;
            }
        }
        if (0.55 < elev && elev < 0.61) {
            return Terrains.getByName("Gravel");
        }
        if (elev >= 0.61f) {
            return Terrains.getByName((String) RocksFromGrid.rockDefAt(x, y).getProperty("naturalTerrain").getStat());
        }
        terrain2 = TerrainThreshold.terrainAtValue(tile.biome.thresholdsByFertility, fert);
        if (terrain2 != null) {
            return terrain2;
        }
        return Terrains.getByName("Sand");
    }
}
