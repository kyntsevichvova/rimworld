package com.rimworld.engine.map.local;

import com.rimworld.engine.Permission;

import java.util.List;

public class Stockpile {
    private String name;
    private int priority;
    private int MinQuality;
    private int MaxQuality;
    private int MinCondition;
    private int MaxCondition;
    private Area area;

    private Permission permission;

    public String getName() {
        return name;
    }

    public void setName(String name) throws IllegalArgumentException {
        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException();
        }
        this.name = name.trim();
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) throws IllegalArgumentException {
        if (!(0 <= priority && priority < 10)) {
            throw new IllegalArgumentException("Priority should be in [0..9]");
        }
        this.priority = priority;
    }

    public void add(List<Point> points) {
        area.add(points);
    }

    public void remove(List<Point> points) {
        area.remove(points);
    }
}
