package com.rimworld.engine.map.local;

import com.rimworld.engine.NameString;
import com.rimworld.engine.PropertyHolder;
import com.rimworld.engine.entity.statics.things.Thing;

public class Surface extends PropertyHolder {
    private NameString name;
    private Thing originalMaterial;
    private Thing flooring;
}
