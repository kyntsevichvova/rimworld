package com.rimworld.engine.map.local;

import com.rimworld.engine.Temperature;
import com.rimworld.engine.entity.statics.StaticEntity;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.local.terrain.Terrain;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/*public class LocalMap {
    public LocalTile[][] tiles;
    public double[][] elevation;
    public double[][] fertility;

    private GlobalTile tileInfo;

    public LocalMap(GlobalTile tileInfo) {
        this.tileInfo = tileInfo;
        //tiles = TileGenerator.getTile(tileInfo);

    }
}*/

public class LocalMap {
    public LocalTile[][] tiles;
    public double[][] elevation;
    public double[][] fertility;
    public Terrain[][] terrains;

    public GlobalTile tileInfo;

    public final static List<Point> offsets4 = Arrays.asList(
            new Point(-1, 0), new Point(0, 1),
            new Point(1, 0), new Point(0, -1)
    );

    public final static List<Point> offsets8 = Arrays.asList(
            new Point(-1, 0), new Point(0, 1),
            new Point(1, 0), new Point(0, -1),
            new Point(-1, -1), new Point(-1, 1),
            new Point(1, -1), new Point(1, 1)
    );

    private int id;
    private int mapSize;
    //private LocalTile[][] tiles;
    private Map<StaticEntity, Point> staticEntities;
    private Map<Integer, Stockpile> stockpiles;
    private TemperatureMap temperatureMap;
    private LightMap lightMap;

    public LocalMap(GlobalTile tileInfo) {
        this.tileInfo = tileInfo;
        //tiles = TileGenerator.getTile(tileInfo);

    }

    public LocalMap(int mapSize) {
        tiles = new LocalTile[mapSize][mapSize];
        this.mapSize = mapSize;
        // Generate tiles using Global Tile
        temperatureMap = new TemperatureMap(mapSize);
        lightMap = new LightMap(mapSize);
    }

    public int getId() {
        return id;
    }

    public LocalTile getTile(Point position) {
        return tiles[position.getX()][position.getY()];
    }

    public Temperature getTemperature(Point position) {
        return temperatureMap.get(position);
    }

    public int getLight(Point position) {
        return lightMap.get(position);
    }

    public boolean checkEnclosed(Point position) {
        boolean[][] used = new boolean[mapSize][mapSize];
        for (int i = 0; i < mapSize; i++) {
            for (int j = 0; j < mapSize; j++) {
                used[i][j] = false;
            }
        }
        ArrayDeque<Point> q = new ArrayDeque<>();
        q.addLast(position);
        used[position.getX()][position.getY()] = true;
        boolean mapEndReached = false;
        while (!q.isEmpty() && !mapEndReached) {
            Point v = q.removeFirst();
            for (Point offset : offsets4) {
                Point next = v.add(offset);
                if (!next.isInBorders(0, mapSize - 1, 0, mapSize - 1)) {
                    mapEndReached = true;
                    break;
                }
                if (used[next.getX()][next.getY()]) {
                    continue;
                }
                if (getTile(next).isPassable()) {
                    q.addLast(next);
                    used[next.getX()][next.getY()] = true;
                }
            }
        }
        return !mapEndReached;
    }

    public Area getEnclosedArea(Point position) {
        if (!checkEnclosed(position)) {
            return null;
        }
        return null;
    }
}
