package com.rimworld.engine.map.local.terrain;

import com.rimworld.engine.PropertyHolder;

import java.io.File;
import java.util.List;

public class TerrainThreshold extends PropertyHolder {
    public Terrain terrain;
    public double min = -1000;
    public double max = +1000;

    public TerrainThreshold(File f) {
        super(f);
        this.terrain = Terrains.getByName((String)getProperty("terrain").getStat());
        this.min = (Double) (getProperty("min")).getStat();
        this.max = (Double) (getProperty("max")).getStat();
    }

    public static Terrain terrainAtValue(List<TerrainThreshold> thresholdsByFertility, double fert) {
        for (TerrainThreshold threshold : thresholdsByFertility) {
            if (threshold.min <= fert && fert <= threshold.max) {
                return threshold.terrain;
            }
        }
        return null;
    }
}
