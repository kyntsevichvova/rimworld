package com.rimworld.engine.map.local.terrain;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Terrains {
    private static List<Terrain> terrains;

    public static void init() {
        terrains = new ArrayList<>();
        readDefs();
    }

    private static void readDefs() {
        try {
            File file = Gdx.files.internal("defs/Terrains").file();
            for (File f : file.listFiles()) {
                terrains.add(new Terrain(f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Terrain getByName(String name) {
        for (Terrain terrain : terrains) {
            if (name.equals(terrain.getProperty("name").getStat())) {
                return terrain;
            }
        }
        return null;
    }
}
