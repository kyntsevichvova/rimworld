package com.rimworld.engine.map.local.terrain;

import com.rimworld.engine.PropertyHolder;
import com.rimworld.engine.generation.modules.ModuleBase;
import com.rimworld.engine.generation.noise.Generator;
import com.rimworld.engine.map.global.GlobalTile;

import java.io.File;

public class TerrainPatchMaker extends PropertyHolder {
    private ModuleBase noise;

    public double minFertility = -999f;
    public double maxFertility = 999f;
    public double frequency;

    public TerrainPatchMaker(File f) {
        super(f);
        this.minFertility = (Double) getProperty("min").getStat();
        this.maxFertility = (Double) getProperty("max").getStat();
    }

    private void init(GlobalTile tile) {
        noise = new Generator(tile.info.randSeeder.nextInt(), 6, (1.0 / frequency), (1.0 / 2.0), 0.5);
    }

    public Terrain terrainAt(int x, int y, GlobalTile tile, double fert) {
        if (fert < minFertility || maxFertility < fert) {
            return null;
        }
        if (noise == null) {
            init(tile);
        }
        /*if (this.minSize > 0) {
            int cnt = 0;
            for ()
        }*/
        return TerrainThreshold.terrainAtValue(tile.biome.thresholdsByFertility, fert);
    }
}
