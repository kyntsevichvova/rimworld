package com.rimworld.engine.map.local.terrain.rocks;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Rocks {
    private static List<Rock> rocks;

    public static void init() {
        rocks = new ArrayList<>();
        readDefs();
    }

    private static void readDefs() {
        try {
            File file = Gdx.files.internal("defs/Rocks").file();
            for (File f : file.listFiles()) {
                rocks.add(new Rock(f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Rock> getAll() {
        return rocks;
    }
}
