package com.rimworld.engine.map.local.terrain.rocks;

import com.rimworld.engine.PropertyHolder;
import com.rimworld.engine.generation.modules.ModuleBase;
import com.rimworld.engine.generation.noise.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RockNoises {
    public static List<RockNoise> noises;

    public static void init(int seed) {
        noises = new ArrayList<>();
        Random seeder = new Random(seed);
        for (PropertyHolder def : Rocks.getAll()) {
            RockNoise rockNoise = new RockNoise();
            rockNoise.rockDef = def;
            rockNoise.noise = new Generator(seeder.nextInt(), 6, (1.0 / 0.005), (1.0 / 2.0), 0.5);
            noises.add(rockNoise);
        }
    }

    public static class RockNoise {
        public PropertyHolder rockDef;
        public ModuleBase noise;

        public RockNoise() {

        }
    }
}
