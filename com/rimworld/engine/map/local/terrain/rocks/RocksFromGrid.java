package com.rimworld.engine.map.local.terrain.rocks;

import com.rimworld.engine.PropertyHolder;

public class RocksFromGrid {
    public static PropertyHolder rockDefAt(int x, int y) {
        PropertyHolder thingDef = null;
        double num = -999999;
        for (int i = 0; i < RockNoises.noises.size(); i++) {
            double value = RockNoises.noises.get(i).noise.getValue(x, y);
            if (value > num) {
                thingDef = RockNoises.noises.get(i).rockDef;
                num = value;
            }
        }
        return thingDef;
    }
}
