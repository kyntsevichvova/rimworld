package com.rimworld.engine.map.local.terrain;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TerrainThresholds {
    private static List<TerrainThreshold> thresholds;

    public static void init() {
        thresholds = new ArrayList<>();
        readDefs();
    }

    private static void readDefs() {
        try {
            File file = Gdx.files.internal("defs/Thresholds").file();
            for (File f : file.listFiles()) {
                thresholds.add(new TerrainThreshold(f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<TerrainThreshold> getAll() {
        return thresholds;
    }

    public static TerrainThreshold getByName(String name) {
        for (TerrainThreshold threshold : thresholds) {
            if (name.equals(threshold.getProperty("name").getStat())) {
                return threshold;
            }
        }
        return null;
    }
}
