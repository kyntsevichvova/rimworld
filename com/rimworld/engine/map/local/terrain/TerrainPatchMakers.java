package com.rimworld.engine.map.local.terrain;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TerrainPatchMakers {
    private static List<TerrainPatchMaker> patchMakers;

    public static void init() {
        patchMakers = new ArrayList<>();
        readDefs();
    }

    private static void readDefs() {
        try {
            File file = Gdx.files.internal("defs/Thresholds").file();
            //File file = Gdx.files.internal("defs/PatchMakers").file();
            for (File f : file.listFiles()) {
                patchMakers.add(new TerrainPatchMaker(f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static TerrainPatchMaker getByName(String name) {
        for (TerrainPatchMaker maker : patchMakers) {
            if (name.equals(maker.getProperty("name").getStat())) {
                return maker;
            }
        }
        return null;
    }
}
