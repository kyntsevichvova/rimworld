package com.rimworld.engine.map.local;

import java.util.List;
import java.util.Set;

public class Area {
    private Set<Point> area;

    public boolean isInArea(Point p) {
        return area.contains(p);
    }

    public void add(List<Point> points) {
        area.addAll(points);
    }

    public void remove(List<Point> points) {
        for (Point p : points) {
            area.remove(p);
        }
    }

    public Set<Point> getPoints() {
        return area;
    }
}
