package com.rimworld.engine.map.local;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point add(Point p) {
        return new Point(x + p.x, y + p.y);
    }

    public boolean isInBorders(int minX, int maxX, int minY, int maxY) {
        return minX <= x && x <= maxX && minY <= y && y <= maxY;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
