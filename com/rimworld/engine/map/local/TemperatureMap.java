package com.rimworld.engine.map.local;

import com.rimworld.engine.Temperature;

public class TemperatureMap {
    private Temperature[][] temperature;

    public TemperatureMap(int mapSize/*, Global Tile*/) {
        temperature = new Temperature[mapSize][mapSize];
        // Generate
    }

    public Temperature get(Point position) {
        return temperature[position.getX()][position.getY()];
    }
}
