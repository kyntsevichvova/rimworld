package com.rimworld.engine.map.local;

public class LightMap {
    private int[][] lightLevel;

    public LightMap(int mapSize/*, Global Tile*/) {
        lightLevel = new int[mapSize][mapSize];
        // Generate lightmap
    }

    public int get(Point position) {
        return lightLevel[position.getX()][position.getY()];
    }
}
