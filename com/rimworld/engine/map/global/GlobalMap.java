package com.rimworld.engine.map.global;

import com.rimworld.engine.world.WorldInfo;

public class GlobalMap {
    public final static int WORLD_HEIGHT = 401;
    public final static int WORLD_WIDTH = 401;

    public GlobalTile[][] tiles;
    public WorldInfo info;

    public GlobalMap(WorldInfo info) {
        this.info = info;
        tiles = TerrainGenerator.getTiles(info);
    }
}
