package com.rimworld.engine.map.global;

import com.rimworld.engine.map.global.biomes.Biome;
import com.rimworld.engine.map.global.biomes.Biomes;
import com.rimworld.engine.map.local.LocalMap;
import com.rimworld.engine.world.WorldInfo;

import java.util.List;

public class GlobalTile {
    private int id;

    private double latitude;
    private double longitude;
    private double elevation;
    private double humidity;
    private double temperature;
    private boolean waterCovered;
    public Biome biome;
    public WorldInfo info;

    private LocalMap localMap;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getElevation() {
        return elevation;
    }

    public void setElevation(double elevation) {
        this.elevation = elevation;
        if (elevation < 0) {
            this.waterCovered = true;
        }
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void determineBiome() {
        List<Biome> biomes = Biomes.getAll();
        Biome biome = null;
        double score = 0;
        for (Biome biome1 : biomes) {
            double s = biome1.getScore(this);
            if (s > score || biome == null) {
                score = s;
                biome = biome1;
            }
        }
        this.biome = biome;
    }

    public boolean isWaterCovered() {
        return waterCovered;
    }

    public double getSwampiness() {
        return 0;
    }

    public Biome getBiome() {
        return biome;
    }

    public void setID(int i) {
        id = i;
    }

    public int getID() {
        return id;
    }
}
