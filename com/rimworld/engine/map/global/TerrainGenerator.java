package com.rimworld.engine.map.global;

import com.rimworld.engine.generation.noise.Generator;
import com.rimworld.engine.generation.modules.*;
import com.rimworld.engine.generation.th.OverallRainfall;
import com.rimworld.engine.generation.th.OverallRainfallUtility;
import com.rimworld.engine.generation.th.OverallTemperature;
import com.rimworld.engine.generation.th.OverallTemperatureUtility;
import com.rimworld.engine.world.WorldInfo;

import java.util.Arrays;
import java.util.Random;

import static com.rimworld.engine.map.global.GlobalMap.WORLD_HEIGHT;
import static com.rimworld.engine.map.global.GlobalMap.WORLD_WIDTH;

public class TerrainGenerator {
    private static final double ELEVATION_SPAN = 4400;
    private static final double ELEVATION_MIN = -400;
    private static ModuleBase elevationGen;
    private static ModuleBase humidityGen;
    private static ModuleBase averageTemperatureByLatitude;
    private static ModuleBase temperatureGen;
    private static ModuleBase temperatureOffsetGen;
    private static Random randSeeder;
    private static Random rand;

    public static GlobalTile[][] getTiles(WorldInfo info) {
        randSeeder = info.randSeeder;
        rand = new Random(randSeeder.nextInt());
        GlobalTile[][] tiles = new GlobalTile[WORLD_HEIGHT][WORLD_WIDTH];
        setupElevationGen();
        setupHumidityGen(info.humidityModifier);
        setupTemperatureGen(info.temperatureModifier);
        setupTemperatureOffsetGen();

        for (int i = 0; i < WORLD_HEIGHT; i++) {
            for (int j = 0; j < WORLD_WIDTH; j++) {
                tiles[i][j] = new GlobalTile();
                GlobalTile tile = tiles[i][j];
                tile.info = info;
                tile.setID(i * WORLD_HEIGHT + j);

                tile.setLatitude(90.0 * (i - WORLD_HEIGHT / 2) / (WORLD_HEIGHT / 2));
                tile.setLongitude(180.0 * (j - WORLD_WIDTH / 2) / (WORLD_WIDTH / 2));

                tile.setElevation(getElevation(tile));
                tile.setHumidity(getHumidity(tile));
                tile.setTemperature(getTemperature(tile));
                tile.determineBiome();
            }
        }
        return tiles;
    }

    private static void setupElevationGen() {
        ModuleBase lhs = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.035), (1.0 / 2.0), 0.40000000596046448);
        ModuleBase moduleBase = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.012), (1.0 / 2.0), 0.5);
        ModuleBase moduleBase2 = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.12), (1.0 / 2.0), 0.5);
        ModuleBase moduleBase3 = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.01), (1.0 / 2.0), 0.5);

        double num = 0.10 * rand.nextDouble() + 0.15;
        moduleBase2 = new ScaleBias(0.5, 0.5, moduleBase2);
        moduleBase = new Multiply(moduleBase, moduleBase2);

        double num2 = 0.2 * rand.nextDouble() + 0.4;
        elevationGen = new Blend(lhs, moduleBase, new Const(num2));
        elevationGen = new Blend(elevationGen, moduleBase3, new Const(num));

        elevationGen = new ScaleBias(0.5, 0.5, elevationGen);
        elevationGen = new Power(elevationGen, new Const(3.0));
        elevationGen = new ScaleBias(ELEVATION_SPAN, ELEVATION_MIN, elevationGen);
    }

    private static void setupHumidityGen(OverallRainfall modifier) {
        ModuleBase moduleBase = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.015), (1.0 / 2.0), 0.5);
        moduleBase = new ScaleBias(0.5, 0.5, moduleBase);
        ModuleBase moduleBase2 =
                new AbsX(new AbsY(
                        new Curve(Arrays.asList(
                                new CurvePoint(0, 1.12),
                                new CurvePoint(25, 0.94),
                                new CurvePoint(45, 0.7),
                                new CurvePoint(70, 0.3),
                                new CurvePoint(80, 0.05),
                                new CurvePoint(90, 0.05)
                        ))
                ));
        humidityGen = new Multiply(moduleBase, moduleBase2);

        double num = 0.000222222225;
        double num2 = -500 * num;
        ModuleBase moduleBase3 = new ScaleBias(num, num2, elevationGen);
        moduleBase3 = new ScaleBias(-1.0, 1.0, moduleBase3);
        moduleBase3 = new Clamp(0.0, 1.0, moduleBase3);
        humidityGen = new Multiply(humidityGen, moduleBase3);
        humidityGen = new RainfallProcessor(humidityGen);

        humidityGen = new Power(humidityGen, new Const(1.5));
        humidityGen = new Clamp(0, 999, humidityGen);

        humidityGen = new ScaleBias(4000.0, 0.0, humidityGen);
        ModuleBase curve = OverallRainfallUtility.getCurve(modifier);
        if (curve != null) {
            humidityGen = new CurveSimple(humidityGen, curve);
        }
    }

    private static void setupTemperatureGen(OverallTemperature modifier) {
        averageTemperatureByLatitude = new Curve(Arrays.asList(
                new CurvePoint(0, 30),
                new CurvePoint(9, 29),
                new CurvePoint(30, 15),
                new CurvePoint(45, 7),
                new CurvePoint(60, -10),
                new CurvePoint(90, -37)
        ));
        temperatureGen = OverallTemperatureUtility.getCurve(modifier);
    }

    private static void setupTemperatureOffsetGen() {
        temperatureOffsetGen = new Generator(randSeeder.nextInt(), 6, (1.0 / 0.015), (1.0 / 2.0), 0.5);
        temperatureOffsetGen = new Multiply(temperatureOffsetGen, new Const(4));
    }

    private static double getBaseTemperatureAtLatitude(double latitude) {
        double x = Math.abs(latitude);
        return averageTemperatureByLatitude.getValue(x, 0);
    }

    private static double getTemperatureReductionAtElevation(double elevation) {
        if (elevation < 250.0) {
            return 0.0;
        }
        double t = (elevation - 250.0) / 4750.0;
        return 30.0 * t;
    }

    private static double getElevation(GlobalTile tile) {
        return elevationGen.getValue(tile.getLatitude(), tile.getLongitude());
    }

    private static double getHumidity(GlobalTile tile) {
        return humidityGen.getValue(tile.getLatitude(), tile.getLongitude());
    }

    private static double getTemperature(GlobalTile tile) {
        double num = getBaseTemperatureAtLatitude(tile.getLatitude());
        num -= getTemperatureReductionAtElevation(tile.getElevation());
        num += temperatureOffsetGen.getValue(tile.getLatitude(), tile.getLongitude());
        if (temperatureGen != null) {
            num = temperatureGen.getValue(num, 0);
        }
        return num;
    }
}
