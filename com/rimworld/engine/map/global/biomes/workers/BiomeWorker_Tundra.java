package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_Tundra implements BiomeWorker {
    @Override
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        return -tile.getTemperature();
    }
}
