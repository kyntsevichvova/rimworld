package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_TemperateSwamp implements BiomeWorker {
    @Override
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        if (tile.getTemperature() < -10.0) {
            return 0.0;
        }
        if (tile.getHumidity() < 600.0) {
            return 0.0;
        }
        if (tile.getSwampiness() < 0.5) {
            return 0.0;
        }
        return 15.0 + (tile.getTemperature() - 7.0) + (tile.getHumidity() - 600.0) / 180.0 + tile.getSwampiness() * 3.0;
    }
}
