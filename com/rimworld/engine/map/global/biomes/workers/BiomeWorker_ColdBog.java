package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_ColdBog implements BiomeWorker {
    @Override
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        if (tile.getTemperature() < -10.0) {
            return 0.0;
        }
        if (tile.getSwampiness() < 0.5) {
            return 0.0;
        }
        return -tile.getTemperature() + 13.0 + tile.getSwampiness() * 8.0;
    }
}
