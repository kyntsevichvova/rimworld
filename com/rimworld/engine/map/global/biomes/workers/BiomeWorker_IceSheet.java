package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_IceSheet implements BiomeWorker {
    @Override
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        return -20.0 + (-tile.getTemperature() * 2.0);
    }
}
