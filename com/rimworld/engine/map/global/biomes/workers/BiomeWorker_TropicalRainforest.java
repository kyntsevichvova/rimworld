package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_TropicalRainforest implements BiomeWorker {
    @Override
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        if (tile.getTemperature() < 15.0) {
            return 0.0;
        }
        if (tile.getHumidity() < 2000.0) {
            return 0.0;
        }
        return 28.0 + (tile.getTemperature() - 20.0) * 1.5 + (tile.getHumidity() - 600.0) / 165.0;
    }
}
