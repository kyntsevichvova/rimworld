package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public class BiomeWorker_BorealForest implements BiomeWorker {
    public double getScore(GlobalTile tile) {
        if (tile.isWaterCovered()) {
            return -100.0;
        }
        if (tile.getTemperature() < -10.0) {
            return 0.0;
        }
        if (tile.getHumidity() < 600.0) {
            return 0.0;
        }
        return 15.0;
    }
}
