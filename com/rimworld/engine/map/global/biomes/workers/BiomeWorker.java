package com.rimworld.engine.map.global.biomes.workers;

import com.rimworld.engine.map.global.GlobalTile;

public interface BiomeWorker {
    double getScore(GlobalTile tile);
}
