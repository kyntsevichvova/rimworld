package com.rimworld.engine.map.global.biomes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.rimworld.engine.PropertyHolder;
import com.rimworld.engine.map.global.GlobalTile;
import com.rimworld.engine.map.global.biomes.workers.BiomeWorker;
import com.rimworld.engine.map.local.terrain.TerrainPatchMaker;
import com.rimworld.engine.map.local.terrain.TerrainPatchMakers;
import com.rimworld.engine.map.local.terrain.TerrainThreshold;
import com.rimworld.engine.map.local.terrain.TerrainThresholds;
import com.rimworld.game.RimworldGame;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class Biome extends PropertyHolder {
    private BiomeWorker worker;
    public Sprite sprite;
    public List<TerrainPatchMaker> terrainPatchMakers;
    public List<TerrainThreshold> thresholdsByFertility;

    public Biome(File file) {
        super(file);
        try {
            Class<?> clazz = Class.forName((String) getProperty("worker").getStat());
            Constructor<?> constructor = clazz.getConstructor();
            this.worker = (BiomeWorker) constructor.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            sprite = new Sprite(
                    RimworldGame.atlas.findRegion((String) getProperty("region").getStat())
            );
            sprite.setSize(5, 5);
        } catch (Exception e) {
            System.out.println(file.getName());
            e.printStackTrace();
            try {
                sprite = new Sprite(
                    new Texture(Gdx.files.internal((String) getProperty("texture").getStat()))
                );
                sprite.setSize(5, 5);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        terrainPatchMakers = new ArrayList<>();
        thresholdsByFertility = new ArrayList<>();
        try {
            String tbfs = (String) getProperty("tbfs").getStat();
            String tpms = (String) getProperty("tpms").getStat();
            double f = (Double) getProperty("tpmsF").getStat();

            StringBuilder sb = new StringBuilder();
            for (Character c : tbfs.toCharArray()) {
                if (c.equals('#')) {
                    thresholdsByFertility.add(TerrainThresholds.getByName(sb.toString()));
                    sb = new StringBuilder();
                } else {
                    sb.append(c);
                }
            }
            thresholdsByFertility.add(TerrainThresholds.getByName(sb.toString()));

            sb = new StringBuilder();
            for (Character c : tpms.toCharArray()) {
                if (c.equals('#')) {
                    terrainPatchMakers.add(TerrainPatchMakers.getByName(sb.toString()));
                    terrainPatchMakers.get(terrainPatchMakers.size() - 1).frequency = f;
                    sb = new StringBuilder();
                } else {
                    sb.append(c);
                }
            }
            terrainPatchMakers.add(TerrainPatchMakers.getByName(sb.toString()));
        } catch (Exception e) {

        }
    }

    public double getScore(GlobalTile tile) {
        return worker.getScore(tile);
    }
}
