package com.rimworld.engine.map.global.biomes;

import com.badlogic.gdx.Gdx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Biomes {
    private static List<Biome> biomes;

    public static void readDefs() {
        try {
            File file = Gdx.files.internal("defs/Biomes").file();
            for (File f : file.listFiles()) {
                biomes.add(new Biome(f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void init() {
        biomes = new ArrayList<>();
        readDefs();
    }

    public static List<Biome> getAll() {
        return biomes;
    }
}
