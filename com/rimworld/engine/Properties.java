package com.rimworld.engine;

public class Properties {
    public final static String WALKABLE_SPEED = "walkable_speed";
    public final static String FERTILITY = "fertility";
    public final static String SLEEPING_SPOT = "slleping_spot";
    public final static String SITTING_SPOT = "sitting_spot";
    public final static String MEETING_SPOT = "meeting_spot";
    public final static String MAX_PAWNS_USING = "max_pawns_using";
}
