package com.rimworld.engine;

public class Property<T> {
    private NameString name;
    private T stat;

    public Property(NameString name, T stat) {
        this.name = name;
        this.stat = stat;
    }

    public NameString getName() {
        return name;
    }

    public T getStat() {
        return stat;
    }
}
