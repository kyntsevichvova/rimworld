package com.rimworld.engine;

import java.util.ArrayList;
import java.util.List;

public class Permission {
    public enum Allowed {
        Nothing, Something, Everything
    }

    public enum NodeType {
        Group, Property, Leaf
    }

    private NameString name;
    private NodeType node;
    private Allowed allowed;
    private List<Permission> children;
    private Permission parent;

    public Permission(Permission parent) {
        this.parent = parent;
        children = new ArrayList<>();
    }

    public Permission() {
        this(null);
    }

    public void addChild(Permission child) {
        children.add(child);
    }

    public boolean isLeaf() {
        return this.node == NodeType.Leaf;
    }

    public boolean isProperty() {
        return this.node == NodeType.Property;
    }

    public boolean isGroup() {
        return this.node == NodeType.Group;
    }

    public Allowed getAllowed() {
        return this.allowed;
    }

    public void checkAllowed() {
        int nothing = 0;
        int everything = 0;
        for (Permission child : children) {
            if (child.getAllowed() == Allowed.Everything) {
                everything++;
            } else if (child.getAllowed() == Allowed.Nothing) {
                nothing++;
            }
        }
        if (everything == children.size()) {
            this.setAllowed(Allowed.Everything);
        } else if (nothing == children.size()) {
            this.setAllowed(Allowed.Nothing);
        } else {
            this.setAllowed(Allowed.Something);
        }
    }

    public void setAllowed(Allowed allowed) throws IllegalArgumentException {
        if (!isGroup() && allowed == Allowed.Something) {
            throw new IllegalArgumentException("Trying to partially allow leaf or property");
        }
        this.allowed = allowed;
        if (isGroup()) {
            if (allowed == Allowed.Everything) {
                for (Permission child : children) {
                    child.setAllowed(Allowed.Everything);
                }
            } else if (allowed == Allowed.Nothing) {
                for (Permission child : children) {
                    child.setAllowed(Allowed.Nothing);
                }
            }
        }
        if (parent != null) {
            parent.checkAllowed();
        }
    }
}
