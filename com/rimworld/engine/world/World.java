package com.rimworld.engine.world;

import java.util.Random;

public class World {
    public final static int TICKS_PER_DAY = 60_000;
    public final static int TICKS_PER_SEC = 60;

    private static String seed;
    private static Random rnd;
    private static int ticksFromStart;

    public World(String seed) {
        World.seed = seed;
        World.rnd = new Random(seed.hashCode());
    }

    public static int getRandomNumber(int l, int r) {
        return l + rnd.nextInt(r - l + 1);
    }

    public int getCurrentDayTick() {
        return ticksFromStart % TICKS_PER_DAY;
    }
}
