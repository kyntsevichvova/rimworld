package com.rimworld.engine.world;

import com.rimworld.engine.generation.th.OverallRainfall;
import com.rimworld.engine.generation.th.OverallTemperature;

import java.util.Random;

public class WorldInfo {
    public OverallRainfall humidityModifier;
    public OverallTemperature temperatureModifier;
    public String worldSeed;
    public int seed;
    public int size;
    public Random randSeeder;

    public WorldInfo() {

    }

    public WorldInfo(String seed, int humidityModifier, int temperatureModifier) {
        this.worldSeed = seed;
        this.seed = seed.hashCode();
        this.humidityModifier = getHumidityModifier(humidityModifier);
        this.temperatureModifier = getTemperatureModifier(temperatureModifier);
        this.size = 250;
        this.randSeeder = new Random(this.seed);
    }

    public void setSeed(String seed) {
        this.worldSeed = seed;
        this.seed = seed.hashCode();
        this.randSeeder = new Random(this.seed);
    }

    public static OverallTemperature getTemperatureModifier(int temperature) {
        if (temperature <= 1) {
            return OverallTemperature.VeryCold;
        } else if (temperature <= 2) {
            return OverallTemperature.Cold;
        } else if (temperature <= 3) {
            return OverallTemperature.Normal;
        } else if (temperature <= 4) {
            return OverallTemperature.Hot;
        } else {
            return OverallTemperature.VeryHot;
        }
    }

    public static OverallRainfall getHumidityModifier(int humidity) {
        if (humidity <= 1) {
            return OverallRainfall.AlmostNone;
        } else if (humidity <= 2) {
            return OverallRainfall.Little;
        } else if (humidity <= 3) {
            return OverallRainfall.Normal;
        } else if (humidity <= 4) {
            return OverallRainfall.High;
        } else {
            return OverallRainfall.VeryHigh;
        }
    }
}
